<?php

use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Business\BusinessAdminController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Consumer\ConsumerController;
use App\Http\Controllers\Product\ProductAdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Trace\TraceController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', [UserController::class, 'index'])->name('user.home');

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    

    Route::group(['prefix'=> 'user'], function () {
        Route::get('/dashboard', [UserController::class, 'dashboard'])->name('user.dashboard');
        Route::get('/product', [UserController::class, 'product'])->name('user.product');
        Route::get('/product/{id}', [UserController::class, 'productView'])->name('user.product.view');
    });
});

Route::group(['prefix'=> 'user', 'middleware' => 'redirectAdmin'], function () {
    Route::get('/login', [AdminAuthController::class, 'showLoginForm'])->name('user.login');
    Route::get('/register', [AdminAuthController::class, 'showRegisterForm'])->name('user.register');
    Route::post('/login', [AdminAuthController::class, 'login'])->name('user.login.post');
    Route::post('/register', [AdminAuthController::class, 'register'])->name('user.register.post');
    Route::post('/logout', [AdminAuthController::class, 'logout'])->name('user.logout');
});


Route::middleware(['auth', 'admin'])->prefix('admin')->group(function() {
    Route::group(['prefix'=> 'product'], function () {
        Route::get('/', [ProductAdminController::class, 'index'])->name('admin.product.index');
        Route::post('/', [ProductAdminController::class, 'create'])->name('admin.product.create');
        Route::patch('/{id}', [ProductAdminController::class, 'update'])->name('admin.product.update');
        Route::delete('/{id}', [ProductAdminController::class, 'delete'])->name('admin.product.delete');
    });

    Route::group(['prefix'=> 'business'], function () {
        Route::get('/', [BusinessAdminController::class, 'index'])->name('admin.business.index');
        Route::post('/', [BusinessAdminController::class, 'create'])->name('admin.business.create');
        Route::patch('/{id}', [BusinessAdminController::class, 'update'])->name('admin.business.update');
        Route::delete('/{id}', [BusinessAdminController::class, 'delete'])->name('admin.business.delete');
    });

    Route::group(['prefix'=> 'category'], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('admin.category.index');
        Route::post('/', [CategoryController::class, 'create'])->name('admin.category.create');
        Route::patch('/{id}', [CategoryController::class, 'update'])->name('admin.category.update');
        Route::delete('/{id}', [CategoryController::class, 'delete'])->name('admin.category.delete');
    });
});

require __DIR__.'/auth.php';
