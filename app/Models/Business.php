<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model 
{

    protected $casts = [
        'user_id' => 'int'
    ];

    protected $fillable = [
        'name',
        'user_id',
        'type',
        'location',
        'actor_name',
        'actor_image'
    ];

}
