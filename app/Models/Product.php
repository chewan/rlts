<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model 
{
    use Filterable;

    protected $casts = [
        'category_id' => 'int',
        'user_id' => 'int',
        'business_id' => 'int',
    ];

    protected $fillable = [
        'name',
        'user_id',
        'business_id',
        'quantity',
        'image',
        'price',
        'price_per_kg',
        'product_arrived',
        'expired_date',
        'category_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function business(): BelongsTo
    {
        return $this->belongsTo(Business::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
