<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'quantity' => 'bail|nullable|integer|min:0',
            'price' => 'bail|nullable|numeric|min:0',
            'price_per_kg' => 'bail|nullable|numeric|min:0',
            'product_arrived' => 'bail|nullable|date',
            'expired_date' => 'bail|nullable|date',
            'category_id' => 'bail|nullable|integer|min:0',
            'business_id' => 'bail|nullable|integer|min:0',
        ];
    }

   
}
