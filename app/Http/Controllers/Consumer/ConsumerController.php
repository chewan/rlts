<?php

namespace App\Http\Controllers\Consumer;

use App\Enums\Category;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Inertia\Inertia;
use Illuminate\Http\Request;

class ConsumerController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::with('business')
            ->filter($request->all())
            ->latest()
            ->get()
            ->map(function ($product) {
                return [
                    'id' => $product->id,
                    'name' => $product->name,
                    'category_id' => [
                        'id' => $product->category_id,
                        'description' => $product->category_id->description(),
                    ],
                    'quantity' => $product->quantity,
                    'price' => $product->price,
                    'price_per_kg' => $product->price_per_kg,
                    'image' => asset($product->image),
                    'product_arrived' => $product->product_arrived,
                    'expired_date' => $product->expired_date,
                    'business' => $product->business,
                ];
            });

        $categories = Category::getCollection();

        return Inertia::render('Consumer/Index', ['products' => $products, 'categories' => $categories]);
    }
}
