<?php

namespace App\Http\Controllers\Trace;

use App\Enums\Category;
use App\Filters\ProductFilter;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Inertia\Inertia;
use Illuminate\Http\Request;

class TraceController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::with('business')->filter($request->all())->get();
        $categories = Category::getCollection();

        return Inertia::render('User/Trace/Index', ['products' => $products, 'categories' => $categories]);
    }
}
