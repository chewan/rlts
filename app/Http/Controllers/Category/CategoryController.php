<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Business\BusinessUpdateRequest;
use App\Http\Requests\Category\CategoryUpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::latest()->get();

        return Inertia::render('Admin/Category/Index', ['categories' => $categories]);
    }

    public function create(Request $request)
    {
        Category::create(
            [
                'name' => $request->name,
            ]
        );

        return redirect()->route('admin.category.index')->with('success', 'Category created successfully');
    }

    public function update(CategoryUpdateRequest $request, $id)
   {
        $data = $request->validated();
        $category = Category::findOrFail($id);

        $category->update($data);

        return redirect()->route('admin.category.index')->with('success', 'Category updated successfully');
   }

   public function delete($id)
   {
        Category::findOrFail($id)->delete();

        return redirect()->route('admin.category.index')->with('success', 'Category updated successfully');
   }
}