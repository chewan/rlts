<?php

namespace App\Http\Controllers\User;

use App\Enums\Category;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Inertia\Inertia;

class UserController extends Controller
{
    public function index()
    {
        return Inertia::render('User/Index', [
            'canLogin' => app('router')->has('login'),
            'canRegister' => app('router')->has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
        ]);
    }

    public function dashboard(Request $request)
    {
        return Inertia::render('Consumer/Index');
    }

    public function product(Request $request)
    {
        $products = Product::with('business')
            ->filter($request->all())
            ->latest()
            ->get()
            ->map(function ($product) {
                return [
                    'id' => $product->id,
                    'name' => $product->name,
                    'category_id' => [
                        'id' => $product->category ? $product->category->id : '',
                        'description' => $product->category ? $product->category->name : '',
                    ],
                    'quantity' => $product->quantity,
                    'price' => $product->price,
                    'price_per_kg' => $product->price_per_kg,
                    'image' => asset($product->image),
                    'product_arrived' => $product->product_arrived,
                    'expired_date' => $product->expired_date,
                    'business' => $product->business,
                ];
            });

        $categories = Category::getCollection();

        return Inertia::render('Consumer/Product', ['products' => $products, 'categories' => $categories]);
    }

    public function productView($id)
    {
        $product = Product::whereId($id)
            ->with('business')
            ->first();

        if ($product) {
            $product = [
                'id' => $product->id,
                'name' => $product->name,
                'category_id' => [
                    'id' => $product->category ? $product->category->id : '',
                    'description' => $product->category ? $product->category->name : '',
                ],
                'quantity' => $product->quantity,
                'price' => $product->price,
                'price_per_kg' => $product->price_per_kg,
                'image' => asset($product->image),
                'product_arrived' => $product->product_arrived,
                'expired_date' => $product->expired_date,
                'business' => $product->business,
            ];
        }

        $categories = Category::getCollection();

        return Inertia::render('Consumer/ProductView', ['product' => $product, 'categories' => $categories]);
    }
}
