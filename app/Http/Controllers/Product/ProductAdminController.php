<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Models\Business;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;

class ProductAdminController extends Controller
{
   public function index() 
   {
        $products = Product::whereUserId(auth()->user()->id)
            ->latest()
            ->get()
            ->map(function ($product) {
                return [
                    'id' => $product->id,
                    'name' => $product->name,
                    'category_id' => [
                        'id' => $product->category ? $product->category->id : '',
                        'description' => $product->category ? $product->category->name : '',
                    ],
                    'quantity' => $product->quantity,
                    'business_id' => $product->business_id,
                    'price' => $product->price,
                    'price_per_kg' => $product->price_per_kg,
                    'image' => asset($product->image),
                    'product_arrived' => $product->product_arrived,
                    'expired_date' => $product->expired_date,
                    'business' => $product->business,
                ];
            });

        $categories = Category::get();
        $businesses = Business::whereUserId(auth()->user()->id)->get();

        return Inertia::render('Admin/Product/Index', ['products' => $products, 'categories' => $categories, 'businesses' => $businesses]);
   }

   public function create(Request $request)
   {
        $product = Product::create([
            'name' => $request->name,
            'user_id' => auth()->user()->id,
            'category_id' => $request->category_id,
            'business_id' => $request->business_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
            'price_per_kg' => $request->price_per_kg,
            'product_arrived' => $request->product_arrived,
            'expired_date' => $request->expired_date,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $uniqueName = time() .'-'. Str::random(10) .'.'. $image->getClientOriginalExtension();
            $image->move('product_image', $uniqueName);
            $product->update([
                'image' => 'product_image/'. $uniqueName ,
            ]);
        }

        return redirect()->route('admin.product.index')->with('success', 'Product created successfully');
   }

   public function update(ProductUpdateRequest $request, $id)
   {
        $data = $request->validated();
        $product = Product::findOrFail($id);

        $product->update($data);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $uniqueName = time() .'-'. Str::random(10) .'.'. $image->getClientOriginalExtension();
            $image->move('product_image', $uniqueName);
            $product->update([
                'image' => 'product_image/'. $uniqueName ,
            ]);
        }

        return redirect()->route('admin.product.index')->with('success', 'Product updated successfully');
   }

   public function delete($id)
   {
         Product::findOrFail($id)->delete();

        return redirect()->route('admin.product.index')->with('success', 'Product updated successfully');
   }
}
