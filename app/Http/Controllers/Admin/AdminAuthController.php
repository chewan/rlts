<?php

namespace App\Http\Controllers\Admin;

use App\Enums\Type;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Hash;

class AdminAuthController extends Controller
{
    public function showLoginForm() 
    {
        return Inertia::render('User/Login');
    }

    public function showRegisterForm() 
    {
        $types = Type::getCollection();

        return Inertia::render('User/Register', ['types' => $types]);
    }

    public function login(Request $request) 
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('user.dashboard');
        }

        return redirect()->route('user.login')->with('error', 'Invalid credentials.');
    }

    public function register(Request $request) 
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|lowercase|email|max:255|unique:'.User::class,
            'type' => 'required|int',
            'password' => ['required'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'isAdmin' => $request->type
        ]);

        Auth::login($user);

        return redirect()->route('user.dashboard');

        return redirect()->route('user.register')->with('error', 'Invalid credentials.');
    }

    public function logout(Request $request) 
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();

        return redirect()->route('user.home');
    }
}
