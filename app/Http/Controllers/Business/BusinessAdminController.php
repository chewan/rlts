<?php

namespace App\Http\Controllers\Business;

use App\Http\Controllers\Controller;
use App\Http\Requests\Business\BusinessUpdateRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Models\Business;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;

class BusinessAdminController extends Controller
{
    public function index()
    {
        $businesses = Business::whereUserId(auth()->user()->id)->latest()->get();

        return Inertia::render('Admin/Business/Index', ['businesses' => $businesses]);
    }

    public function create(Request $request)
    {
        $business = Business::create(
            [
                'user_id' => auth()->user()->id,
                'actor_name' => $request->actor_name,
                'name' => $request->name,
                'type' => $request->type,
                'location' => $request->location,
            ]
        );

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $uniqueName = time() .'-'. Str::random(10) .'.'. $image->getClientOriginalExtension();
            $image->move('actor_image', $uniqueName);
            $business->update([
                'actor_image' => 'actor_image/'. $uniqueName ,
            ]);
        }

        return redirect()->route('admin.business.index')->with('success', 'Business created successfully');
    }

    public function update(BusinessUpdateRequest $request, $id)
   {
        $data = $request->validated();
        $business = Business::findOrFail($id);

        $business->update($data);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $uniqueName = time() .'-'. Str::random(10) .'.'. $image->getClientOriginalExtension();
            $image->move('actor_image', $uniqueName);
            $business->update([
                'actor_image' => 'actor_image/'. $uniqueName ,
            ]);
        }

        return redirect()->route('admin.business.index')->with('success', 'Business updated successfully');
   }

   public function delete($id)
   {
        Business::findOrFail($id)->delete();

        return redirect()->route('admin.business.index')->with('success', 'Business updated successfully');
   }
}