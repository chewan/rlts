<?php

namespace App\Traits;

trait EnumReturnable
{
    public static function getCollection()
    {
        return collect(self::cases())->map(fn ($case) => [
            'value' => $case->value,
            'key' => $case->name,
            'description' => method_exists($case, 'description') ? $case->description() : $case->name,
        ]);
    }
}
