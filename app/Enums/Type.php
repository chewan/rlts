<?php

namespace App\Enums;

use App\Traits\EnumReturnable;

enum Type: int
{
    use EnumReturnable;

    case Consumer = 0;
    case Business = 1;

    public function description(): string
    {
        return match ($this) {
            self::Consumer => "Consumer",
            self::Business => "Business",
        };
    }
}
