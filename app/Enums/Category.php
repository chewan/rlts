<?php

namespace App\Enums;

use App\Traits\EnumReturnable;

enum Category: int
{
    use EnumReturnable;

    case FreshProduct = 0;
    case MeatAndPoultry = 1;
    case Seafood = 2;
    case DairyProducts = 3;
    case BakeryIngredients = 4;
    case DryGoods = 5;
    case CannedaAndPackagedFoods = 6;
    case SpicesAndSeasonings = 7;
    case Beverages = 8;
    case FrozenFoods = 9;
    case OrganicAndHealthFoods = 10;
    case SpecialtyIngredients = 11;
    case BakingSupplies = 12;
    case SnackFoods = 13;
    case HouseholdEssentials = 14;

    public function description(): string
    {
        return match ($this) {
            self::FreshProduct => "Fresh Product",
            self::MeatAndPoultry => "Meat and Poultry",
            self::Seafood => "Seafood",
            self::DairyProducts => "Dairy Products",
            self::BakeryIngredients => "Bakery Ingredients",
            self::DryGoods => "Dry Goods",
            self::CannedaAndPackagedFoods => "Canned and Packaged Foods",
            self::SpicesAndSeasonings => "Spices and Seasonings",
            self::Beverages => "Beverages",
            self::FrozenFoods => "Frozen Foods",
            self::OrganicAndHealthFoods => "Organic and Health Foods",
            self::SpecialtyIngredients => "Specialty Ingredients",
            self::BakingSupplies => "Baking Supplies",
            self::SnackFoods => "Snack Foods",
            self::HouseholdEssentials => "Household Essentials",
        };
    }
}
