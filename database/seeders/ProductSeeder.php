<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::create([
            'name' => 'Wagyu A5',
            'user_id' => 1,
            'quantity' => 10,
            'price' => 1200.00,
            'price_per_kg' => 120.00,
            'product_arrived' => Carbon::now(),
            'expired_date' => Carbon::now()
        ]);

        Product::create([
            'name' => 'Wagyu A4',
            'user_id' => 1,
            'quantity' => 10,
            'price' => 1200.00,
            'price_per_kg' => 120.00,
            'product_arrived' => Carbon::now(),
            'expired_date' => Carbon::now()
        ]);

        Product::create([
            'name' => 'Wagyu A3',
            'user_id' => 1,
            'quantity' => 10,
            'price' => 1200.00,
            'price_per_kg' => 120.00,
            'product_arrived' => Carbon::now(),
            'expired_date' => Carbon::now()
        ]);

        Product::create([
            'name' => 'Wagyu A2',
            'user_id' => 1,
            'quantity' => 10,
            'price' => 1200.00,
            'price_per_kg' => 120.00,
            'product_arrived' => Carbon::now(),
            'expired_date' => Carbon::now()
        ]);
    }
}
