<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();

        Category::truncate();

        $now = Carbon::now();

        $categories = [
            ['name' => 'Fresh Product', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Meat and Poultry', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Seafood', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Dairy Products', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Bakery Ingredients', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Dry Goods', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Canned and Packaged Foods', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Spices and Seasonings', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Beverages', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Frozen Foods', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Organic and Health Foods', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Specialty Ingredients', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Baking Supplies', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Snack Foods', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Household Essentials', 'created_at' => $now, 'updated_at' => $now],
        ];

        foreach ($categories as $category) {
            Category::updateOrCreate($category);
        }

        Schema::enableForeignKeyConstraints();
    }
}
