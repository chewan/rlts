Install required packages
```bash
composer install
```

Setup environment configuration and update it accordingly (db, app key etc)
```bash
cp .env.example .env
```

Migrate DB and seeder
```bash
php artisan migrate:fresh --seed
```
> Set the db config before run migration

Storage Link
```bash
php artisan storage:link
```

- PHP 8.2^
- Composer 2.5.4^
- [Eloquent Filter](https://github.com/Tucker-Eric/EloquentFilter) - An Eloquent way to filter Eloquent Models and their relationships.
